###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                             CP - Control Pump (SINAMICS DRIVE)                           ##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 1.4             ################################
# Author:  Attila Horvath
# Date:    08-08-2022
# Version: v1.4
# Changes: 
# 1. Archived PVs updated     
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.3
# Changes: 
# 1. Variable Name Unification 
############################         Version: 1.2             ################################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes: 
# 1. Major review, with Sinamics drive test
# 2. Indent,  unit standardization
############################           Version: 1.1           ################################
# Author:  Miklos Boros, Marino Vojneski
# Date:    2018-07-20
# Version: v1.1
# Changes: Removed blocks for alarms from auxiliary devices.
############################           Version: 1.0           ################################
# Author:  Miklos Boros, Marino Vojneski
# Date:    2018-07-18
# Version: v1.0
##############################################################################################



############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",  					ARCHIVE=True,           PV_DESC="Operation Mode Auto",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("OpMode_Manual",					ARCHIVE=True,           PV_DESC="Operation Mode Manual",    PV_ONAM="True",                          PV_ZNAM="False")
add_digital("OpMode_Forced",					ARCHIVE=True,           PV_DESC="Operation Mode Forced",    PV_ONAM="True",                          PV_ZNAM="False")

#Pump states
add_analog("PumpActSpeed",             "REAL",  ARCHIVE=True,           PV_DESC="Pump Actual Speed",             PV_EGU="rpm")
add_analog("PumpSP",                   "REAL",  ARCHIVE=True,           PV_DESC="Pump Setpoint",                 PV_EGU="rpm")
add_analog("PumpOUTFreq",              "REAL",  ARCHIVE=True,           PV_DESC="Pump Output Frequency",         PV_EGU="Hz")
add_analog("PumpOUTVoltage",           "REAL",  ARCHIVE=True,           PV_DESC="Pump Output Voltage",           PV_EGU="V")
add_analog("PumpOUTCurrent",           "REAL",  ARCHIVE=True,           PV_DESC="Pump Output Current",           PV_EGU="A")
add_analog("PumpOUTPower",             "REAL",  ARCHIVE=True,           PV_DESC="Pump Output Power",             PV_EGU="kW")
add_analog("PumpPUTemp",               "REAL",  ARCHIVE=True,           PV_DESC="Pump Power Unit Temp",          PV_EGU="degC")
add_analog("PumpMaxRPM",               "INT",   ARCHIVE=True,           PV_DESC="Pump Motor Maximal Speed",      PV_EGU="rpm")
add_analog("OperHours",                "REAL",  ARCHIVE=True,           PV_DESC="Pump Operating Hours",          PV_EGU="h")
add_analog("StartState",               "INT",   ARCHIVE=True,           PV_DESC="Pump Statemachine START")
add_analog("StopState",                "INT",   ARCHIVE=True,           PV_DESC="Pump Statemachine STOP")
add_analog("RestartState",             "INT",   ARCHIVE=True,           PV_DESC="Pump Statemachine RESET")
add_digital("Accelerating",            									PV_DESC="Pump Accelerating",        PV_ONAM="Accelerating",                  PV_ZNAM="NotMoving")
add_digital("Decelerating",            PV_DESC="Pump Decelerating",        PV_ONAM="Decelerating",                  PV_ZNAM="NotMoving")

#Status Word
add_digital("RdyForSwitchON",  					ARCHIVE=True,          PV_DESC="Ready for switching on",   PV_ONAM="Drive_Power_ON",                PV_ZNAM="Drive_Power_OFF")            #STATUS_WORD.0  
add_digital("Ready",  							ARCHIVE=True,          PV_DESC="Pump Ready for Start",     PV_ONAM="Ready",                         PV_ZNAM="NotReady")                   #STATUS_WORD.1  
add_digital("Running",  						ARCHIVE=True,          PV_DESC="Pump Running",             PV_ONAM="Running",                       PV_ZNAM="NotRunning")                 #STATUS_WORD.2  
add_digital("CoastStop",  						ARCHIVE=True,          PV_DESC="Coast Stop OFF2",          PV_ONAM="CoastStop_ON",                  PV_ZNAM="CoastStop_OFF")              #STATUS_WORD.4CoastStop is active when 0  
add_digital("QuickStop",  						ARCHIVE=True,          PV_DESC="Quick Stop OFF3",          PV_ONAM="QuickStop_ON",                  PV_ZNAM="QuickStop_OFF")              #STATUS_WORD.5QuickStop is active when 0  
add_digital("Inhibited",  						ARCHIVE=True,          PV_DESC="Switching ON Inhibited",   PV_ONAM="Inhibited_ON",                  PV_ZNAM="Inhibited_OFF")              #STATUS_WORD.6  
add_digital("RemoteControlReq",  				ARCHIVE=True,          PV_DESC="Remote Control Requested", PV_ONAM="RemoteControlReq",              PV_ZNAM="NoRemoteControlReq")         #STATUS_WORD.9  
add_digital("MaxSpeedReached",  				ARCHIVE=True,          PV_DESC="Maximum speed reached",    PV_ONAM="MaxSpeed",                      PV_ZNAM="BelowMax")                   #STATUS_WORD.10


#Inhibit signals (set by the PLC code, can't be changed by the OPI) 
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",      PV_ONAM="InhibitManual",                 PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",       PV_ONAM="InhibitForce",                  PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",          PV_ONAM="InhibitLocking",                PV_ZNAM="AllowLocking")
add_digital("LatchAlarm",              PV_DESC="Latching of alarms",       PV_ONAM="Latched",                       PV_ZNAM="NotLatched")
add_digital("KeepSP",                  PV_DESC="Keep SP from AutoMode",    PV_ONAM="KeepSP",                        PV_ZNAM="DoNotKeep")

#Interlock signals
add_digital("GroupInterlock",          PV_DESC="Group Interlock",          PV_ONAM="True",                          PV_ZNAM="False")
add_string("InterlockMsg", 39,                       PV_NAME="InterlockMsg",               PV_DESC="Interlock Message")
add_digital("StartInterlock",          PV_DESC="Start Interlock",          PV_ONAM="True",                          PV_ZNAM="False")
add_digital("StopInterlock",           PV_DESC="Stop Interlock",           PV_ONAM="True",                          PV_ZNAM="False")


#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",       PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",     PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableSTARTBtn",          PV_DESC="Enable Start Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableSTOP2Btn",          PV_DESC="Enable Stop2 Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableSTOP3Btn",          PV_DESC="Enable Stop3 Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_analog("MotorColor",               "INT",                              PV_DESC="Motor BlockIcon COlor")

#Vibration
add_analog("Vibration",                "REAL",  ARCHIVE=True,                             PV_DESC="Vibration value",               PV_EGU="mm/s2")

#Vibration
add_analog("VB_ScaleLOW",              "REAL",  ARCHIVE=True,                             PV_DESC="VB_Scale LOW",                  PV_EGU="mm/s2")
add_analog("VB_ScaleHIGH",             "REAL",  ARCHIVE=True,                             PV_DESC="VB_Scale HIGH",                 PV_EGU="mm/s2")

#Alarm signals
add_major_alarm("GroupAlarm",          "Group Alarm",                      PV_DESC="Group Alarm",                   PV_ONAM="GroupAlarm",        PV_ZNAM="NominalState")
add_major_alarm("Overheated",          "Overheated",                       PV_DESC="Drive Overheated",              PV_ONAM="Alarm",             PV_ZNAM="NoAlarm")               
add_major_alarm("Alarm",               "Drive Alarm",                      PV_DESC="Drive Alarm is present",        PV_ONAM="Alarm",             PV_ZNAM="NoAlarm")                    #STATUS_WORD.3  
add_minor_alarm("Warning",             "Drive Warning",                    PV_DESC="Drive Warning is present",      PV_ONAM="Warning",           PV_ZNAM="NoWarning")                  #STATUS_WORD.7
add_major_alarm("SPDiscrepancy",       "SP Discrepancy",                   PV_DESC="SetPoint Discrepancy",          PV_ONAM="SP_Discrepancy",    PV_ZNAM="SP_OK")                      #STATUS_WORD.8
add_major_alarm("TorqueLimit",         "Torque Limit",                     PV_DESC="Pump Torque Limit Reached",     PV_ONAM="IMPlimitreached",   PV_ZNAM="NominalState")              #STATUS_WORD.11
add_major_alarm("DriveOverLoad",       "Drive Overload",                   PV_DESC="Drive Overload",                PV_ONAM="Overload",          PV_ZNAM="NominalState")             #STATUS_WORD.15
add_major_alarm("ModuleDiagError",     "Module Diagnostics",               PV_DESC="HW PN Module Error",            PV_ONAM="Module Diag Error", PV_ZNAM="NominalState")
add_major_alarm("DriveExternal",       "DriveExternal",                    PV_DESC="Drive stopped by external event",PV_ONAM="Drive Error",      PV_ZNAM="NominalState")
add_major_alarm("SSTriggered","SSTriggered",                               PV_ZNAM="NominalState")

add_major_alarm("VB_HIHI",             "Vibration HIHI",                   PV_DESC="Vibrations HIHI",               PV_ZNAM="NominalState")
add_minor_alarm("VB_HI",               "Vibration HI",                     PV_DESC="Vibrations HI",                 PV_ZNAM="NominalState")
add_major_alarm("VB_IO_Error",         "Vibration IP error",               PV_DESC="Vibrations IO Error",           PV_ZNAM="NominalState")
add_major_alarm("VB_Module_Error",     "Vibration Module error",           PV_DESC="Vibrations Module Error",       PV_ZNAM="NominalState")


#Ramping
add_analog("MaxRampUPSpd",             "REAL",                             PV_DESC="Maximum Ramping UP Speed",      PV_EGU="rpm/s")
add_analog("MaxRampDNSpd",             "REAL",                             PV_DESC="Maximum Ramping DOWN Speed",    PV_EGU="rpm/s")
add_analog("ActRampSpeed",             "REAL",                             PV_DESC="Actual Ramping Speed",          PV_EGU="rpm/s")
add_digital("Ramping",                 PV_DESC="Ramping Indicator",        PV_ONAM="True",                          PV_ZNAM="False")
add_digital("RampSettingOK",           PV_DESC="Ramping can be enabled",   PV_ONAM="True",                          PV_ZNAM="False")


#Feedbacks
add_analog("FB_Setpoint",              "REAL",                             PV_DESC="FB Setpoint from HMI (SP)",     PV_EGU="rpm")
add_analog("FB_Step",                  "REAL",                             PV_DESC="FB Step value for open close",  PV_EGU="rpm")
#Ramping
add_analog("FB_RampUPTIME",            "INT",                              PV_DESC="Ramping UP time",               PV_EGU="sec")
add_analog("FB_RampUPRANGE",           "REAL",                             PV_DESC="Ramping UP range",              PV_EGU="rpm")
add_analog("FB_RampDNTIME",            "INT",                              PV_DESC="Ramping DOWN time",             PV_EGU="sec")
add_analog("FB_RampDNRANGE",           "REAL",                             PV_DESC="Ramping DOWN range",            PV_EGU="rpm")
#Vibration
add_analog("VB_FB_Limit_HIHI",         "REAL",                             PV_DESC="VB Feedback Limit HIHI",        PV_EGU="mm/s2")
add_analog("VB_FB_Limit_HI",           "REAL",                             PV_DESC="VB Feedback Limit HI",          PV_EGU="mm/s2")

############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                ARCHIVE=True,		PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              ARCHIVE=True,		PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",               ARCHIVE=True,		PV_DESC="CMD: Force Mode")

add_digital("Cmd_RampON",              ARCHIVE=True,		PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",             ARCHIVE=True,		PV_DESC="Turn Ramping OFF")


add_digital("Cmd_AckAlarm",            ARCHIVE=True,		PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_PumpStart",           ARCHIVE=True,		PV_DESC="CMD: Pump Start")
add_digital("Cmd_Stop",                ARCHIVE=True,		PV_DESC="CMD: Pump Stop")
add_digital("Cmd_RampedStop",          ARCHIVE=True,		PV_DESC="CMD: Pump Ramped Stop")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Setpoint and Manipulated value from HMI
add_analog("P_Setpoint",               "REAL", ARCHIVE=True,               PV_DESC="Setpoint from HMI (SP)",        PV_EGU="rpm")

#Limits
add_analog("VB_P_Limit_HIHI",          "REAL", ARCHIVE=True,               PV_DESC="VB Limit HIHI",                 PV_EGU="mm/s2")
add_analog("VB_P_Limit_HI",            "REAL", ARCHIVE=True,               PV_DESC="VB Limit HI",                   PV_EGU="mm/s2")

add_analog("P_Step",                   "REAL",                             PV_DESC="Step value for Accel./Deccel.", PV_EGU="%")

#Ramping speed from the HMI
add_analog("P_RampUPTIME",             "INT",  ARCHIVE=True,               PV_DESC="Ramping UP Time",               PV_EGU="sec")
add_analog("P_RampUPRANGE",            "REAL", ARCHIVE=True,               PV_DESC="Ramping UP Range",              PV_EGU="rpm")
add_analog("P_RampDNTIME",             "INT",  ARCHIVE=True,               PV_DESC="Ramping DOWN Time",             PV_EGU="sec")
add_analog("P_RampDNRANGE",            "REAL", ARCHIVE=True,               PV_DESC="Ramping DOWN Range",            PV_EGU="rpm")
