# Author:  Adalberto Fontoura
# Date:    10-05-2023
# Version: v1.0

############################
#  STATUS BLOCK
############################ 
define_status_block()

#Analyzer values
add_analog("VL_L1-N",                   "REAL",                         PV_DESC="Voltage L1-N",            ARCHIVE=True,             PV_EGU="V")
add_analog("VL_L2-N",                   "REAL",                         PV_DESC="Voltage L2-N",            ARCHIVE=True,             PV_EGU="V")
add_analog("VL_L3-N",                   "REAL",                         PV_DESC="Voltage L3-N",            ARCHIVE=True,             PV_EGU="V")
add_analog("VL_L1-L2",                  "REAL",                         PV_DESC="Voltage L1-L2",           ARCHIVE=True,             PV_EGU="V")
add_analog("VL_L2-L3",                  "REAL",                         PV_DESC="Voltage L2-L3",           ARCHIVE=True,             PV_EGU="V")
add_analog("VL_L3-L1",                  "REAL",                         PV_DESC="Voltage L1-L3",           ARCHIVE=True,             PV_EGU="V")
add_analog("Curr_I1_L1",                "REAL",                         PV_DESC="Current I L1",            ARCHIVE=True,             PV_EGU="A")
add_analog("Curr_I2_L2",                "REAL",                         PV_DESC="Current I L2",            ARCHIVE=True,             PV_EGU="A")
add_analog("Curr_I3_L3",                "REAL",                         PV_DESC="Current I L3",            ARCHIVE=True,             PV_EGU="A")
add_analog("Vect_Curr_Sum_I1I2I3",      "REAL",                         PV_DESC="Vector sun I1I2I3",       ARCHIVE=True,             PV_EGU="A")
add_analog("Real_Pwr_P1_L1N",           "REAL",                         PV_DESC="Real power P1 L1N",       ARCHIVE=True,             PV_EGU="W")
add_analog("Real_Pwr_P2_L2N",           "REAL",                         PV_DESC="Real power P2 L2N",       ARCHIVE=True,             PV_EGU="W")
add_analog("Real_Pwr_P3_L3N",           "REAL",                         PV_DESC="Real power P3 L3N",       ARCHIVE=True,             PV_EGU="W")
add_analog("RP_Sum_P1P2P3",             "REAL",                         PV_DESC="Sum P1P2P3",              ARCHIVE=True,             PV_EGU="W")
add_analog("Frequency",                 "REAL",                         PV_DESC="Measured Frequency",      ARCHIVE=True,             PV_EGU="Hz")
add_analog("THD_L1N_U0L1",              "REAL",                         PV_DESC="Harmonic THD U L1-N",     ARCHIVE=True,             PV_EGU="%")     
add_analog("THD_L2N_U0L2",              "REAL",                         PV_DESC="Harmonic THD U L2-N",     ARCHIVE=True,             PV_EGU="%")     
add_analog("THD_L3N_U0L3",              "REAL",                         PV_DESC="Harmonic THD U L3-N",     ARCHIVE=True,             PV_EGU="%")     
add_analog("THDI1_I1_I01",              "REAL",                         PV_DESC="Harmonic THD I L1",       ARCHIVE=True,             PV_EGU="%")     
add_analog("THDI2_I2_I02",              "REAL",                         PV_DESC="Harmonic THD I L2",       ARCHIVE=True,             PV_EGU="%")     
add_analog("THDI3_I3_I03",              "REAL",                         PV_DESC="Harmonic THD I L3",       ARCHIVE=True,             PV_EGU="%")

############################
#  COMMAND BLOCK
############################ 
define_command_block()


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
