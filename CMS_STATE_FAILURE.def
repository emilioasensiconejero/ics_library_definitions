###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                       Failure Action                                     ##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 1.1             ################################
# Author:  Attila Horvath
# Date:    08-08-2022
# Version: v1.1
# Changes: 
# 1. Archived PVs updated     
############################ Version: 1.0             ########################################
# Author:  Miklos Boros 
# Date:    06-05-2020
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Step Messages
add_string("StrMsg1", 39,         PV_NAME="StrMsg1",            PV_DESC="Step Message 1")
add_string("StrMsg2", 39,         PV_NAME="StrMsg2",            PV_DESC="Step Message 2")


#Status BITs
add_digital("STS_Inactive",  	ARCHIVE=True,         	PV_DESC="Failure Action Inactive",        		PV_ONAM="True",         PV_ZNAM="False")
add_digital("STS_Active",  		ARCHIVE=True,         	PV_DESC="Failure Action Active",          		PV_ONAM="True",         PV_ZNAM="False")
add_digital("STS_Operator",  	ARCHIVE=True,         	PV_DESC="Operator Action",            			PV_ONAM="True",         PV_ZNAM="False")
add_digital("STS_Timeout",  	ARCHIVE=True,         	PV_DESC="Step Timeout",	            			PV_ONAM="True",                       PV_ZNAM="False")
add_analog("STS_StepID","INT",  ARCHIVE=True,         	PV_DESC="Unique ID")
add_analog("Actions_GMP","WORD",       					PV_DESC="Failure Action Executions")
add_analog("Actions_CBX1","WORD",       				PV_DESC="Failure Action Executions")
add_analog("Actions_CBX2","WORD",       				PV_DESC="Failure Action Executions")
add_analog("Actions_JSB","WORD",       					PV_DESC="Failure Action Executions")
add_analog("Actions_DB","WORD",       					PV_DESC="Failure Action Executions")
add_analog("Actions_OPMS","WORD",       				PV_DESC="Failure Action Executions")
add_analog("Actions_VAC1","WORD",       				PV_DESC="Failure Action Executions")
add_analog("Actions_VAC2","WORD",       				PV_DESC="Failure Action Executions")
add_analog("Actions_WaC","WORD",       					PV_DESC="Failure Action Executions")
add_analog("Actions_GMP_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_CBX1_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_CBX2_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_JSB_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_DB_Mask","WORD",       				PV_DESC="Failure Action Mask")
add_analog("Actions_OPMS_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_VAC1_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_VAC2_Mask","WORD",       			PV_DESC="Failure Action Mask")
add_analog("Actions_WaC_Mask","WORD",       			PV_DESC="Failure Action Mask")



#Initial condition
add_analog("STS_ActStep","INT", ARCHIVE=True,           PV_DESC="Actual Step ID")

#State dependent Interlock
add_minor_alarm("StateInterlock",     "StateInterlock Active",                                    								PV_ZNAM="True")

#Active BlockIcon Buttons
add_digital("BTN_Act_Ena",            					PV_DESC="Activate Button Enable",          		PV_ONAM="True",         PV_ZNAM="False")
add_digital("BTN_Act_Disa",           					PV_DESC="Activate Button Disable",         		PV_ONAM="True",         PV_ZNAM="False")

#Flag bits
add_digital("Flag1",  			ARCHIVE=True,           PV_DESC="Flag1",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag2",  			ARCHIVE=True,           PV_DESC="Flag2",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag3",  			ARCHIVE=True,           PV_DESC="Flag3",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag4",  			ARCHIVE=True,           PV_DESC="Flag4",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag5",  			ARCHIVE=True,           PV_DESC="Flag5",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag6",  			ARCHIVE=True,           PV_DESC="Flag6",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag7",  			ARCHIVE=True,           PV_DESC="Flag7",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag8",  			ARCHIVE=True,           PV_DESC="Flag8",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag9",  			ARCHIVE=True,           PV_DESC="Flag9",      							PV_ONAM="True",         PV_ZNAM="False")
add_digital("Flag10", 			ARCHIVE=True,           PV_DESC="Flag10",     							PV_ONAM="True",         PV_ZNAM="False")


#State alarms
add_digital("GroupAlarm",             					PV_DESC="Group Alarm",                     		PV_ONAM="True",         PV_ZNAM="False")
add_major_alarm("Alarm1",            "Alarm1",                                                    								PV_ZNAM="True")
add_major_alarm("Alarm2",            "Alarm2",                                                    								PV_ZNAM="True")
add_major_alarm("Alarm3",            "Alarm3",                                                    								PV_ZNAM="True")
add_major_alarm("Alarm4",            "Alarm4",                                                    								PV_ZNAM="True")
add_major_alarm("Alarm5",            "Alarm5",                                                    								PV_ZNAM="True")


add_digital("BTN_YES_ENA",            PV_DESC="BTN_YES_ENA",     PV_ONAM="True",                  PV_ZNAM="False")
add_digital("BTN_OK_ENA",             PV_DESC="BTN_OK_ENA",      PV_ONAM="True",                  PV_ZNAM="False")
add_digital("BTN_NO_ENA",             PV_DESC="BTN_NO_ENA",      PV_ONAM="True",                  PV_ZNAM="False")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("CMD_Activate",           ARCHIVE=True,		PV_DESC="CMD_Activate",        PV_ONAM="True",              PV_ZNAM="False")
add_digital("CMD_DeActivate",         ARCHIVE=True,		PV_DESC="CMD_DeActivate",      PV_ONAM="True",              PV_ZNAM="False")

add_digital("CMD_YES",                ARCHIVE=True,		PV_DESC="CMD_YES",    PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_OK",                 ARCHIVE=True,		PV_DESC="CMD_OK",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("CMD_NO",                 ARCHIVE=True,		PV_DESC="CMD_NO",     PV_ONAM="True",                       PV_ZNAM="False")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

